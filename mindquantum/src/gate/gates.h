/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MINDQUANTUM_GATE_GATES_H_
#define MINDQUANTUM_GATE_GATES_H_
#include "utils.h"
#include "gate/base_gate.h"
#include "matrix/two_dim_matrix.h"
#include <cmath>
#include <string>

namespace mindquantum {

template <typename T>
BaseGate<T> XGate = {
  parameterized_ : false,
  name_ : gX,
  hermitian_prop_ : SELFHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{0, 0}, {1, 0}}, {{1, 0}, {0, 0}}}}
};

template <typename T>
BaseGate<T> YGate = {
  parameterized_ : false,
  name_ : gY,
  hermitian_prop_ : SELFHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{0, 0}, {0, -1}}, {{0, 1}, {0, 0}}}}
};

template <typename T>
BaseGate<T> ZGate = {
  parameterized_ : false,
  name_ : gZ,
  hermitian_prop_ : SELFHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{1, 0}, {0, 0}}, {{0, 0}, {-1, 0}}}}
};

template <typename T>
BaseGate<T> IGate = {
  parameterized_ : false,
  name_ : gI,
  hermitian_prop_ : SELFHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{1, 0}, {0, 0}}, {{0, 0}, {1, 0}}}}
};

template <typename T>
BaseGate<T> HGate = {
  parameterized_ : false,
  name_ : gH,
  hermitian_prop_ : SELFHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{static_cast<T>(M_SQRT1_2), 0}, {static_cast<T>(M_SQRT1_2), 0}},
                                {{static_cast<T>(M_SQRT1_2), 0}, {-static_cast<T>(M_SQRT1_2), 0}}}}
};

template <typename T>
BaseGate<T> TGate = {
  parameterized_ : false,
  name_ : gT,
  hermitian_prop_ : DOHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{1, 0}, {0, 0}}, {{0, 0}, {static_cast<T>(M_SQRT1_2), static_cast<T>(M_SQRT1_2)}}}}
};

template <typename T>
BaseGate<T> SGate = {
  parameterized_ : false,
  name_ : gS,
  hermitian_prop_ : DOHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{1, 0}, {0, 0}}, {{0, 0}, {0, 1}}}}
};

template <typename T>
BaseGate<T> CNOTGate = {
  parameterized_ : false,
  name_ : gCNOT,
  hermitian_prop_ : DOHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{1, 0}, {0, 0}, {0, 0}, {0, 0}},
                                {{0, 0}, {1, 0}, {0, 0}, {0, 0}},
                                {{0, 0}, {0, 0}, {0, 0}, {1, 0}},
                                {{0, 0}, {0, 0}, {1, 0}, {0, 0}}}}
};

template <typename T>
BaseGate<T> CZGate = {
  parameterized_ : false,
  name_ : gCZ,
  hermitian_prop_ : SELFHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{1, 0}, {0, 0}, {0, 0}, {0, 0}},
                                {{0, 0}, {1, 0}, {0, 0}, {0, 0}},
                                {{0, 0}, {0, 0}, {1, 0}, {0, 0}},
                                {{0, 0}, {0, 0}, {0, 0}, {-1, 0}}}}
};

template <typename T>
BaseGate<T> SWAPGate = {
  parameterized_ : false,
  name_ : gSWAP,
  hermitian_prop_ : SELFHERMITIAN,
  base_matrix_ : Dim2Matrix<T>{{{{1, 0}, {0, 0}, {0, 0}, {0, 0}},
                                {{0, 0}, {0, 0}, {1, 0}, {0, 0}},
                                {{0, 0}, {1, 0}, {0, 0}, {0, 0}},
                                {{0, 0}, {0, 0}, {0, 0}, {1, 0}}}}
};

template <typename T>
BaseGate<T> RXGate = {
  parameterized_ : true,
  name_ : gRX,
  hermitian_prop_ : PARAMSOPPOSITE,
  param_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{COS1_2(theta), 0}, {0, -SIN1_2(theta)}}, {{0, -SIN1_2(theta)}, {COS1_2(theta), 0}}}};
  },
  param_diff_matrix_ : [](T theta) {
    return Dim2Matrix<T>{
      {{{-SIN1_2(theta) / 2, 0}, {0, -COS1_2(theta) / 2}}, {{0, -COS1_2(theta) / 2}, {-SIN1_2(theta) / 2, 0}}}};
  }
};

template <typename T>
BaseGate<T> RYGate = {
  parameterized_ : true,
  name_ : gRY,
  hermitian_prop_ : PARAMSOPPOSITE,
  param_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{COS1_2(theta), 0}, {-SIN1_2(theta), 0}}, {{SIN1_2(theta), 0}, {COS1_2(theta), 0}}}};
  },
  param_diff_matrix_ : [](T theta) {
    return Dim2Matrix<T>{
      {{{-SIN1_2(theta) / 2, 0}, {-COS1_2(theta) / 2, 0}}, {{COS1_2(theta) / 2, 0}, {-SIN1_2(theta) / 2, 0}}}};
  }
};

template <typename T>
BaseGate<T> RZGate = {
  parameterized_ : true,
  name_ : gRZ,
  hermitian_prop_ : PARAMSOPPOSITE,
  param_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{COS1_2(theta), -SIN1_2(theta)}, {0, 0}}, {{0, 0}, {COS1_2(theta), SIN1_2(theta)}}}};
  },
  param_diff_matrix_ : [](T theta) {
    return Dim2Matrix<T>{
      {{{-SIN1_2(theta) / 2, -COS1_2(theta) / 2}, {0, 0}}, {{0, 0}, {-SIN1_2(theta) / 2, COS1_2(theta) / 2}}}};
  }
};

template <typename T>
BaseGate<T> PSGate = {
  parameterized_ : true,
  name_ : gPS,
  hermitian_prop_ : PARAMSOPPOSITE,
  param_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{1, 0}, {0, 0}}, {{0, 0}, {COS1_2(2 * theta), SIN1_2(2 * theta)}}}};
  },
  param_diff_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{0, 0}, {0, 0}}, {{0, 0}, {-SIN1_2(2 * theta), COS1_2(2 * theta)}}}};
  }
};

template <typename T>
BaseGate<T> XXGate = {
  parameterized_ : true,
  name_ : gXX,
  hermitian_prop_ : PARAMSOPPOSITE,
  param_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{COS1_2(2 * theta), 0}, {0, 0}, {0, 0}, {0, -SIN1_2(2 * theta)}},
                          {{0, 0}, {COS1_2(2 * theta), 0}, {0, -SIN1_2(2 * theta)}, {0, 0}},
                          {{0, 0}, {0, -SIN1_2(2 * theta)}, {COS1_2(2 * theta), 0}, {0, 0}},
                          {{0, -SIN1_2(2 * theta)}, {0, 0}, {0, 0}, {COS1_2(2 * theta), 0}}}};
  },
  param_diff_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{-SIN1_2(2 * theta), 0}, {0, 0}, {0, 0}, {0, -COS1_2(2 * theta)}},
                          {{0, 0}, {-SIN1_2(2 * theta), 0}, {0, -COS1_2(2 * theta)}, {0, 0}},
                          {{0, 0}, {0, -COS1_2(2 * theta)}, {-SIN1_2(2 * theta), 0}, {0, 0}},
                          {{0, -COS1_2(2 * theta)}, {0, 0}, {0, 0}, {-SIN1_2(2 * theta), 0}}}};
  }
};

template <typename T>
BaseGate<T> YYGate = {
  parameterized_ : true,
  name_ : gYY,
  hermitian_prop_ : PARAMSOPPOSITE,
  param_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{COS1_2(2 * theta), 0}, {0, 0}, {0, 0}, {0, SIN1_2(2 * theta)}},
                          {{0, 0}, {COS1_2(2 * theta), 0}, {0, -SIN1_2(2 * theta)}, {0, 0}},
                          {{0, 0}, {0, -SIN1_2(2 * theta)}, {COS1_2(2 * theta), 0}, {0, 0}},
                          {{0, SIN1_2(2 * theta)}, {0, 0}, {0, 0}, {COS1_2(2 * theta), 0}}}};
  },
  param_diff_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{-SIN1_2(2 * theta), 0}, {0, 0}, {0, 0}, {0, COS1_2(2 * theta)}},
                          {{0, 0}, {-SIN1_2(2 * theta), 0}, {0, -COS1_2(2 * theta)}, {0, 0}},
                          {{0, 0}, {0, -COS1_2(2 * theta)}, {-SIN1_2(2 * theta), 0}, {0, 0}},
                          {{0, COS1_2(2 * theta)}, {0, 0}, {0, 0}, {-SIN1_2(2 * theta), 0}}}};
  }
};

template <typename T>
BaseGate<T> ZZGate = {
  parameterized_ : true,
  name_ : gZZ,
  hermitian_prop_ : PARAMSOPPOSITE,
  param_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{COS1_2(2 * theta), -SIN1_2(2 * theta)}, {0, 0}, {0, 0}, {0, 0}},
                          {{0, 0}, {COS1_2(2 * theta), SIN1_2(2 * theta)}, {0, 0}, {0, 0}},
                          {{0, 0}, {0, 0}, {COS1_2(2 * theta), SIN1_2(2 * theta)}, {0, 0}},
                          {{0, 0}, {0, 0}, {0, 0}, {COS1_2(2 * theta), -SIN1_2(2 * theta)}}}};
  },
  param_diff_matrix_ : [](T theta) {
    return Dim2Matrix<T>{{{{-SIN1_2(2 * theta), -COS1_2(2 * theta)}, {0, 0}, {0, 0}, {0, 0}},
                          {{0, 0}, {-SIN1_2(2 * theta), COS1_2(2 * theta)}, {0, 0}, {0, 0}},
                          {{0, 0}, {0, 0}, {-SIN1_2(2 * theta), COS1_2(2 * theta)}, {0, 0}},
                          {{0, 0}, {0, 0}, {0, 0}, {-SIN1_2(2 * theta), -COS1_2(2 * theta)}}}};
  }
};

template <typename T>
BaseGate<T> GetGateByName(std::string name) {
  BaseGate<T> out;
  if (name == gX) {
    out = XGate<T>;
  } else if (name == gY) {
    out = YGate<T>;
  } else if (name == gZ) {
    out = ZGate<T>;
  } else if (name == gI) {
    out = IGate<T>;
  } else if (name == gH) {
    out = HGate<T>;
  } else if (name == gT) {
    out = TGate<T>;
  } else if (name == gS) {
    out = SGate<T>;
  } else if (name == gCNOT) {
    out = CNOTGate<T>;
  } else if (name == gSWAP) {
    out = SWAPGate<T>;
  } else if (name == gCZ) {
    out = CZGate<T>;
  } else if (name == gRX) {
    out = RXGate<T>;
  } else if (name == gRY) {
    out = RYGate<T>;
  } else if (name == gRZ) {
    out = RZGate<T>;
  } else if (name == gPS) {
    out = PSGate<T>;
  } else if (name == gXX) {
    out = XXGate<T>;
  } else if (name == gYY) {
    out = YYGate<T>;
  } else if (name == gZZ) {
    out = ZZGate<T>;
  }
  return out;
}
}  // namespace mindquantum
#endif  // MINDQUANTUM_GATE_GATES_H_
