/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MINDQUANTUM_GATE_BASE_GATE_H_
#define MINDQUANTUM_GATE_BASE_GATE_H_
#include "utils.h"
#include "pr/parameter_resolver.h"
#include "matrix/two_dim_matrix.h"

#include <string>

namespace mindquantum {
template <typename T>
struct BaseGate {
  bool parameterized_;
  std::string name_;
  VT<Index> obj_qubits_;
  VT<Index> ctrl_qubits_;
  ParameterResolver<T> params_;
  int64_t hermitian_prop_ = SELFHERMITIAN;
  bool daggered_ = false;
  Dim2Matrix<T> base_matrix_;
  Dim2Matrix<T> (*param_matrix_)(T para);
  Dim2Matrix<T> (*param_diff_matrix_)(T para);
  void PrintInfo() {
    if (!daggered_) {
      std::cout << "Gate name: " << name_ << std::endl;
    } else {
      std::cout << "Gate name: " << name_ << " (daggered version)" << std::endl;
    }
    std::cout << "Parameterized: " << parameterized_ << std::endl;
    if (!parameterized_) {
      base_matrix_.PrintInfo();
    }
    if (obj_qubits_.size() != 0) {
      std::cout << "Obj qubits: ";
      for (auto o : obj_qubits_) {
        std::cout << o << " ";
      }
      std::cout << std::endl;
    }
    if (ctrl_qubits_.size() != 0) {
      std::cout << "Control qubits: ";
      for (auto o : ctrl_qubits_) {
        std::cout << o << " ";
      }
      std::cout << std::endl;
    }
  }
  void ApplyValue(T theta) {
    if (parameterized_) {
      parameterized_ = false;
      base_matrix_ = param_matrix_(theta);
    }
  }
  BaseGate() {}
  BaseGate(bool parameterized, std::string name, int64_t hermitian_prop, Dim2Matrix<T> base_matrix)
      : parameterized_(parameterized), name_(name), hermitian_prop_(hermitian_prop), base_matrix_(base_matrix) {}
  BaseGate(bool parameterized, std::string name, int64_t hermitian_prop, Dim2Matrix<T> (*param_matrix)(T para),
           Dim2Matrix<T> (*param_diff_matrix)(T para))
      : parameterized_(parameterized),
        name_(name),
        hermitian_prop_(hermitian_prop),
        param_matrix_(param_matrix),
        param_diff_matrix_(param_diff_matrix) {}
};
}  // namespace mindquantum
#endif  // MINDQUANTUM_GATE_BASE_GATE_H_
