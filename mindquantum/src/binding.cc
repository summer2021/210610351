#include <pybind11/complex.h>
#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>

#include "hamiltonian/hamiltonian.h"
#include "pr/parameter_resolver.h"
#include "sparse/paulimat.h"
#include "sparse/csrhdmatrix.h"
#include "sparse/algo.h"
#include "matrix/two_dim_matrix.h"
#include "gate/gates.h"

#include "backends/projectq/projectq.h"

namespace py = pybind11;
namespace mindquantum {
using mindquantum::sparse::Csr_Plus_Csr;
using mindquantum::sparse::GetPauliMat;
using mindquantum::sparse::PauliMat;
using mindquantum::sparse::PauliMatToCsrHdMatrix;
using mindquantum::sparse::SparseHamiltonian;
using mindquantum::sparse::TransposeCsrHdMatrix;

using mindquantum::projectq::Projectq;

PYBIND11_MODULE(mqbackend, m) {
  // gate
  py::class_<Dim2Matrix<MT>, std::shared_ptr<Dim2Matrix<MT>>>(m, "dim2matrix")
    .def(py::init<>())
    .def(py::init<const VVT<CT<MT>> &>());
  py::class_<BaseGate<MT>, std::shared_ptr<BaseGate<MT>>>(m, "base_gate")
    .def(py::init<>())
    .def(py::init<bool, std::string, int64_t, Dim2Matrix<MT>>())
    .def("PrintInfo", &BaseGate<MT>::PrintInfo)
    .def("apply_value", &BaseGate<MT>::ApplyValue)
    .def_readwrite("obj_qubits", &BaseGate<MT>::obj_qubits_)
    .def_readwrite("ctrl_qubits", &BaseGate<MT>::ctrl_qubits_)
    .def_readwrite("params", &BaseGate<MT>::params_)
    .def_readwrite("daggered", &BaseGate<MT>::daggered_)
    .def_readwrite("hermitian_prop", &BaseGate<MT>::hermitian_prop_);
  m.def("get_gate_by_name", &GetGateByName<MT>);

  // parameter resolver
  py::class_<ParameterResolver<MT>, std::shared_ptr<ParameterResolver<MT>>>(m, "parameter_resolver")
    .def(py::init<const MST<MT> &, const SS &, const SS &>())
    .def(py::init<>())
    .def(py::init<const VT<std::string> &, const VT<MT> &, const VT<bool> &>())
    .def_readonly("data", &ParameterResolver<MT>::data_)
    .def_readonly("no_grad_parameters", &ParameterResolver<MT>::no_grad_parameters_)
    .def_readonly("requires_grad_parameters", &ParameterResolver<MT>::requires_grad_parameters_);

  m.def("linear_combine", &LinearCombine<MT>);

  // pauli mat
  py::class_<PauliMat<MT>, std::shared_ptr<PauliMat<MT>>>(m, "pauli_mat")
    .def(py::init<>())
    .def(py::init<const PauliTerm<MT> &, Index>())
    .def_readonly("n_qubits", &PauliMat<MT>::n_qubits_)
    .def_readonly("dim", &PauliMat<MT>::dim_)
    .def_readwrite("coeff", &PauliMat<MT>::p_)
    .def("PrintInfo", &PauliMat<MT>::PrintInfo);

  m.def("get_pauli_mat", &GetPauliMat<MT>);
  // csr_hd_matrix
  py::class_<CsrHdMatrix<MT>, std::shared_ptr<CsrHdMatrix<MT>>>(m, "csr_hd_matrix")
    .def(py::init<>())
    .def(py::init<Index, Index, py::array_t<Index>, py::array_t<Index>, py::array_t<CT<MT>>>())
    .def("PrintInfo", &CsrHdMatrix<MT>::PrintInfo);
  m.def("csr_plus_csr", &Csr_Plus_Csr<MT>);
  m.def("transpose_csr_hd_matrix", &TransposeCsrHdMatrix<MT>);
  m.def("pauli_mat_to_csr_hd_matrix", &PauliMatToCsrHdMatrix<MT>);

  // hamiltonian
  py::class_<Hamiltonian<MT>, std::shared_ptr<Hamiltonian<MT>>>(m, "hamiltonian")
    .def(py::init<>())
    .def(py::init<const VT<PauliTerm<MT>> &>())
    .def(py::init<const VT<PauliTerm<MT>> &, Index>())
    .def(py::init<std::shared_ptr<CsrHdMatrix<MT>>, Index>())
    .def_readwrite("how_to", &Hamiltonian<MT>::how_to_)
    .def_readwrite("n_qubits", &Hamiltonian<MT>::n_qubits_)
    .def_readwrite("ham", &Hamiltonian<MT>::ham_)
    .def_readwrite("ham_sparse_main", &Hamiltonian<MT>::ham_sparse_main_)
    .def_readwrite("ham_sparse_second", &Hamiltonian<MT>::ham_sparse_second_);
  m.def("sparse_hamiltonian", &SparseHamiltonian<MT>);

  // simulator
  py::class_<Projectq<MT>, std::shared_ptr<Projectq<MT>>>(m, "projectq")
    .def(py::init<>())
    .def(py::init<unsigned, unsigned>())
    .def("apply_gate", py::overload_cast<const BaseGate<MT> &>(&Projectq<MT>::ApplyGate))
    .def("apply_gate",
         py::overload_cast<const BaseGate<MT> &, const ParameterResolver<MT> &, bool>(&Projectq<MT>::ApplyGate))
    .def("apply_circuit", py::overload_cast<const VT<BaseGate<MT>> &>(&Projectq<MT>::ApplyCircuit))
    .def("apply_circuit",
         py::overload_cast<const VT<BaseGate<MT>> &, const ParameterResolver<MT> &>(&Projectq<MT>::ApplyCircuit))
    .def("apply_hamiltonian", &Projectq<MT>::ApplyHamiltonian)
    .def("get_expectation", &Projectq<MT>::GetExpectation)
    .def("oneside_grad", &Projectq<MT>::OnesideGrad)
    .def("PrintInfo", &Projectq<MT>::PrintInfo)
    .def("run", &Projectq<MT>::run)
    .def("get_vec", &Projectq<MT>::cheat)
    .def("hermitian_measure_with_grad",
         py::overload_cast<const VT<Hamiltonian<MT>> &, const VT<BaseGate<MT>> &, const VT<BaseGate<MT>> &,
                           const VT<ParameterResolver<MT>> &, const VT<std::string> &, size_t, size_t>(
           &Projectq<MT>::HermitianMeasureWithGrad))
    .def("hermitian_measure_with_grad",
         py::overload_cast<const VT<Hamiltonian<MT>> &, const VT<BaseGate<MT>> &, const VT<BaseGate<MT>> &,
                           const ParameterResolver<MT> &, const VT<std::string> &, size_t>(
           &Projectq<MT>::HermitianMeasureWithGrad))
    .def("nonhermitian_measure_with_grad",
          py::overload_cast<const VT<Hamiltonian<MT>> &, const VT<Hamiltonian<MT>> &, const VT<BaseGate<MT>> &, const VT<BaseGate<MT>> &,
                            const VT<BaseGate<MT>> &, const VT<BaseGate<MT>> &, const VT<ParameterResolver<MT>> &, 
                            const VT<std::string> &, size_t, size_t>(&Projectq<MT>::NonHermitianMeasureWithGrad))
    .def("nonhermitian_measure_with_grad",
          py::overload_cast<const VT<Hamiltonian<MT>> &, const VT<Hamiltonian<MT>> &, const VT<BaseGate<MT>> &, const VT<BaseGate<MT>> &,
                            const VT<BaseGate<MT>> &, const VT<BaseGate<MT>> &, const ParameterResolver<MT> &, 
                            const VT<std::string> &, size_t>(&Projectq<MT>::NonHermitianMeasureWithGrad));

           
}
}  // namespace mindquantum
