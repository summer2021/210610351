/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef MINDQUANTUM_BACKENDS_PROJECTQ_PROJECTQ_H_
#define MINDQUANTUM_BACKENDS_PROJECTQ_PROJECTQ_H_
#include "backends/projectq/projectq_utils.h"
#include "gate/base_gate.h"
#include "hamiltonian/hamiltonian.h"
#include "pr/parameter_resolver.h"
#include "projectq/backends/_sim/_cppkernels/simulator.hpp"
#include "utils.h"

namespace mindquantum {
namespace projectq {
template <typename T>
class Projectq : public Simulator {
 private:
  unsigned n_qubits_;
  VT<unsigned> ordering_;
  unsigned len_;

 public:
  Projectq() : Simulator(1, 1), n_qubits_(1) {
    for (unsigned i = 0; i < n_qubits_; i++) {
      ordering_.push_back(i);
    }
    len_ = (1UL << (n_qubits_ + 1));
  }

  Projectq(unsigned seed, unsigned N) : Simulator(seed, N), n_qubits_(N) {
    for (unsigned i = 0; i < n_qubits_; i++) {
      ordering_.push_back(i);
    }
    len_ = (1UL << (n_qubits_ + 1));
  }
  Projectq(unsigned seed, unsigned N, calc_type *vec) : Simulator(seed, N), n_qubits_(N) {
    for (unsigned i = 0; i < n_qubits_; i++) {
      ordering_.push_back(i);
    }
    len_ = (1UL << (n_qubits_ + 1));
    memcpy(vec_, vec, sizeof(calc_type) * len_);
  }
  void InitializeSimulator() {
    if (vec_ != NULL) {
      free(vec_);
    }
    vec_ = (StateVector)calloc(len_, sizeof(calc_type));
  }

  void InitializeSimulator(const VT<BaseGate<T>> &circ) {
    Projectq::InitializeSimulator();
    Projectq::ApplyCircuit(circ);
  }

  void InitializeSimulator(CTP<T> vec) {}

  void ApplyGate(const BaseGate<T> &gate) {
    Projectq::apply_controlled_gate(MCast<T>(gate.base_matrix_.matrix_), VCast(gate.obj_qubits_),
                                    VCast(gate.ctrl_qubits_));
  }

  void ApplyGate(const BaseGate<T> &gate, const ParameterResolver<T> &pr, bool diff = false) {
    T theta = LinearCombine(pr, gate.params_);
    if (diff) {
      Projectq::apply_controlled_gate(MCast<T>(gate.param_diff_matrix_(theta).matrix_), VCast(gate.obj_qubits_),
                                      VCast(gate.ctrl_qubits_));
    } else {
      Projectq::apply_controlled_gate(MCast<T>(gate.param_matrix_(theta).matrix_), VCast(gate.obj_qubits_),
                                      VCast(gate.ctrl_qubits_));
    }
  }

  void ApplyCircuit(const VT<BaseGate<T>> &circ) {
    for (auto &gate : circ) {
      Projectq::ApplyGate(gate);
    }
    Projectq::run();
  }

  void ApplyCircuit(const VT<BaseGate<T>> &circ, const ParameterResolver<T> &pr) {
    for (auto &gate : circ) {
      if (gate.parameterized_) {
        Projectq::ApplyGate(gate, pr);
      } else {
        Projectq::ApplyGate(gate);
      }
    }
    Projectq::run();
  }

  void ApplyHamiltonian(const Hamiltonian<T> &ham) {
    Projectq::run();
    if (ham.how_to_ == ORIGIN) {
      auto t1 = NOW();
      Projectq::apply_qubit_operator(HCast<T>(ham.ham_), Projectq::ordering_);
      auto t2 = NOW();
      auto t = TimeDuration(t1, t2);
      std::cout << "ApplyHamiltonian time:" << t << std::endl;
    } else if (ham.how_to_ == BACKEND) {
      Projectq::vec_ = sparse::Csr_Dot_Vec<T, double>(ham.ham_sparse_main_, ham.ham_sparse_second_, Projectq::vec_);
    } else {
      Projectq::vec_ = sparse::Csr_Dot_Vec<T, double>(ham.ham_sparse_main_, Projectq::vec_);
    }
  }

  CT<T> GetExpectation(const Hamiltonian<T> &ham) {
    Projectq<T> sim = Projectq<T>(1, n_qubits_, vec_);
    sim.ApplyHamiltonian(ham);
    auto out = ComplexInnerProduct<T, calc_type>(sim.vec_, vec_, static_cast<Index>(len_));
    return out;
  }

  void OnesideGrad(Projectq<T> &sim_no_grad_side, Projectq<T> &sim_grad_side, 
                          const VT<BaseGate<T>> &circ, const VT<BaseGate<T>> &herm_circ, 
                          const ParameterResolver<T> &pr, MST<size_t> &p_map, VT<CT<T>> &f_g, bool symmetry = true) {
    for (size_t j = 0; j < circ.size(); j++) {
      if ((!herm_circ[j].parameterized_) || (herm_circ[j].params_.requires_grad_parameters_.size() == 0)) {
        if (herm_circ[j].parameterized_) {
          sim_no_grad_side.ApplyGate(herm_circ[j], pr, false);
          sim_grad_side.ApplyGate(herm_circ[j], pr, false);
        } 
        else {
          sim_no_grad_side.ApplyGate(herm_circ[j]);
          sim_grad_side.ApplyGate(herm_circ[j]);
        }
      } 
      else {
        sim_grad_side.ApplyGate(herm_circ[j], pr, false);
        sim_grad_side.run();
        Projectq<T> sim_grad_side_tmp = Projectq<T>(1, n_qubits_, sim_grad_side.vec_);
        sim_grad_side_tmp.ApplyGate(circ[circ.size() - j - 1], pr, true);
        sim_grad_side_tmp.run();
        sim_no_grad_side.run();
        CT<T> gi = 0;
        if (herm_circ[j].ctrl_qubits_.size() == 0) {
          gi = ComplexInnerProduct<T, calc_type>(sim_no_grad_side.vec_, sim_grad_side_tmp.vec_, static_cast<Index>(len_));
        } 
        else {
          gi = ComplexInnerProductWithControl<T, calc_type>(
            sim_no_grad_side.vec_, sim_grad_side_tmp.vec_, static_cast<Index>(len_), GetControlMask(herm_circ[j].ctrl_qubits_));
        }
        if (symmetry){
          for (auto &it : herm_circ[j].params_.requires_grad_parameters_){
            f_g[1 + p_map[it]] -= 2 * herm_circ[j].params_.data_.at(it) * gi;
          }
        }
        else{
          for (auto &it : herm_circ[j].params_.requires_grad_parameters_){
            f_g[1 + p_map[it]] -= herm_circ[j].params_.data_.at(it) * gi;
          }
        }
        sim_no_grad_side.ApplyGate(herm_circ[j], pr, false);
      }
    }
  }


  VT<VT<CT<T>>> HermitianMeasureWithGrad(const VT<Hamiltonian<T>> &hams, const VT<BaseGate<T>> &circ,
                                         const VT<BaseGate<T>> &herm_circ, const ParameterResolver<T> &pr,
                                         const VT<std::string> &params_order, size_t mea_threads) {
    auto n_hams = hams.size();
    auto n_params = pr.data_.size();
    MST<size_t> p_map;
    for (size_t i = 0; i < params_order.size(); i++) {
      p_map[params_order[i]] = i;
    }
    VT<VT<CT<T>>> output(n_hams); 
    Projectq<T> sim = Projectq<T>(1, n_qubits_, vec_);
    sim.ApplyCircuit(circ, pr);
#pragma omp parallel for schedule(static) num_threads(mea_threads)
    for (size_t i = 0; i < n_hams; i++) {
      VT<CT<T>> f_g(n_params + 1, 0);
      Projectq<T> sim_left = Projectq<T>(1, n_qubits_, sim.vec_);
      sim_left.ApplyHamiltonian(hams[i]);
      f_g[0] = ComplexInnerProduct<T, calc_type>(sim.vec_, sim_left.vec_, static_cast<Index>(len_));
      Projectq<T> sim_right = Projectq<T>(1, n_qubits_, sim.vec_);
      Projectq::OnesideGrad(sim_left, sim_right, circ, herm_circ, pr, p_map, f_g);
      output[i] = f_g;
    }
    return output;
  }

  VT<VT<CT<T>>> NonHermitianMeasureWithGrad(const VT<Hamiltonian<T>> &hams, const VT<Hamiltonian<T>> &herm_hams,
                                            const VT<BaseGate<T>> &left_circ, const VT<BaseGate<T>> &herm_left_circ,
                                            const VT<BaseGate<T>> &right_circ, const VT<BaseGate<T>> &herm_right_circ,
                                            const ParameterResolver<T> &pr, const VT<std::string> &params_order,
                                            size_t mea_threads) {
    auto n_hams = hams.size();
    auto n_params = pr.data_.size();
    bool symmetry = false;
    MST<size_t> p_map;
    for (size_t i = 0; i < params_order.size(); i++) {
      p_map[params_order[i]] = i;
    }
    VT<VT<CT<T>>> output(n_hams);
    Projectq<T> sim = Projectq<T>(1, n_qubits_, vec_);
    sim.ApplyCircuit(right_circ, pr);
    Projectq<T> sim2 = Projectq<T>(1, n_qubits_, vec_); 
    sim2.ApplyCircuit(left_circ, pr);
#pragma omp parallel for schedule(static) num_threads(mea_threads)
    for (size_t i = 0; i < n_hams; i++) {
      VT<CT<T>> f_g(n_params + 1, 0);
      {
        Projectq<T> sim_left = Projectq<T>(1, n_qubits_, sim2.vec_);
        sim_left.ApplyHamiltonian(herm_hams[i]);
        f_g[0] = ComplexInnerProduct<T, calc_type>(sim.vec_, sim_left.vec_, static_cast<Index>(len_));
        Projectq<T> sim_right = Projectq<T>(1, n_qubits_, sim.vec_);
        Projectq::OnesideGrad(sim_left, sim_right, right_circ, herm_right_circ, pr, p_map, f_g, symmetry);
      }
      {
        Projectq<T> sim_right = Projectq<T>(1, n_qubits_, sim.vec_);
        sim_right.ApplyHamiltonian(hams[i]);
        f_g[0] = ComplexInnerProduct<T, calc_type>(sim2.vec_, sim_right.vec_, static_cast<Index>(len_));
        Projectq<T> sim_left = Projectq<T>(1, n_qubits_, sim2.vec_);
        Projectq::OnesideGrad(sim_right, sim_left, left_circ, herm_left_circ, pr, p_map, f_g, symmetry);
      }
      output[i] = f_g;
    }
    return output;
  }

  VT<VT<VT<CT<T>>>> HermitianMeasureWithGrad(const VT<Hamiltonian<T>> &hams, const VT<BaseGate<T>> &circ,
                                             const VT<BaseGate<T>> &herm_circ, const VT<ParameterResolver<T>> &prs,
                                             const VT<std::string> &params_order, size_t batch_threads,
                                             size_t mea_threads) {
    auto n_hams = hams.size();
    auto n_prs = prs.size();
    auto n_params = prs[0].data_.size();
    VT<VT<VT<CT<T>>>> output(n_prs);
#pragma omp parallel for schedule(static) num_threads(batch_threads)
    for (size_t i = 0; i < n_prs; i++) {
      Projectq<T> sim = Projectq<T>(1, n_qubits_, vec_);
      auto f_g = sim.HermitianMeasureWithGrad(hams, circ, herm_circ, prs[i], params_order, mea_threads);
      output[i] = f_g;
    }
    return output;
  }

  VT<VT<VT<CT<T>>>> NonHermitianMeasureWithGrad(const VT<Hamiltonian<T>> &hams, const VT<Hamiltonian<T>> &herm_hams,
                                                const VT<BaseGate<T>> &left_circ, const VT<BaseGate<T>> &herm_left_circ,
                                                const VT<BaseGate<T>> &right_circ,
                                                const VT<BaseGate<T>> &herm_right_circ,
                                                const VT<ParameterResolver<T>> &prs,
                                                const VT<std::string> &params_order, size_t batch_threads,
                                                size_t mea_threads) {
    auto n_hams = hams.size();
    auto n_prs = prs.size();
    auto n_params = prs[0].data_.size();
    VT<VT<VT<CT<T>>>> output(n_prs);
#pragma omp parallel for schedule(static) num_threads(batch_threads)
    for (size_t i = 0; i < n_prs; i++) {
      Projectq<T> sim = Projectq<T>(1, n_qubits_, vec_);
      auto f_g = sim.NonHermitianMeasureWithGrad(hams, herm_hams, left_circ, herm_left_circ, right_circ,
                                                 herm_right_circ, prs[i], params_order, mea_threads);
      output[i] = f_g;
    }
    return output;
  }

  void PrintInfo() {
    std::cout << n_qubits_ << " qubits simulator with currently quantum state at:" << std::endl;
    for (unsigned i = 0; i < (len_ >> 1); i++) {
      std::cout << "(" << vec_[2 * i] << ", " << vec_[2 * i + 1] << ")" << std::endl;
    }
  }
};
}  // namespace projectq
}  // namespace mindquantum
#endif  // MINDQUANTUM_BACKENDS_PROJECTQ_PROJECTQ_H_
