# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Hamiltonian module."""

import numpy as np
from numpy.lib.type_check import real
import scipy.sparse as sp
import tqdm
from projectq.ops import QubitOperator as pq_operator
from openfermion.ops import QubitOperator as of_operator
from mindquantum.ops import QubitOperator as hiq_operator
from mindquantum.gate import X, Y, Z, I
from .. import mqbackend as mb

MAT_MAP = {'X': X.matrix(), 'Y': Y.matrix(), 'Z': Z.matrix()}
MODE = {'origin': 0, 'backend': 1, 'frontend': 2}
EDOM = {0: 'origin', 1: 'backend', 2: 'frontend'}


class Hamiltonian:
    """
    A QubitOperator hamiltonian wrapper.

    Args:
        hamiltonian (QubitOperator): The pauli word qubit operator.

    Examples:
        >>> from mindquantum.ops import QubitOperator
        >>> from mindquantum import Hamiltonian
        >>> ham = Hamiltonian(QubitOperator('Z0 Y1', 0.3))
        >>> ham.mindspore_data()
        {'hams_pauli_coeff': [0.3],
         'hams_pauli_word': [['Z', 'Y']],
         'hams_pauli_qubit': [[0, 1]]}
    """
    def __init__(self, hamiltonian):
        if not isinstance(
                hamiltonian,
            (pq_operator, of_operator, hiq_operator, sp.csr_matrix)):
            raise TypeError(
                "Require a QubitOperator or a coo_matrix, but get {}!".format(
                    type(hamiltonian)))
        if isinstance(hamiltonian, sp.csr_matrix):
            self.hamiltonian = hiq_operator('')
            self.sparse_mat = hamiltonian
            self.how_to = MODE['frontend']
            self.n_qubits = int(np.log2(self.sparse_mat.shape[0]))
        else:
            self.hamiltonian = hamiltonian
            self.sparse_mat = sp.csr_matrix(np.eye(2, dtype=np.complex64))
            self.how_to = MODE['origin']
            self.n_qubits = None
        self.ham_termlist = [(i, j) for i, j in self.hamiltonian.terms.items()]

    def __str__(self):
        if not self.how_to:
            return self.hamiltonian.__str__()
        return self.sparse_mat.__str__()

    def __repr__(self):
        if not self.how_to:
            return self.hamiltonian.__repr__()
        return self.sparse_mat.__repr__()

    def sparse(self, n_qubits=1):
        """
        Calculate the sparse matrix of this hamiltonian in pqc operator

        Args:
            n_qubits (int): The total qubit of this hamiltonian, only need when mode is
                'frontend'. Default: 1.
        """
        if EDOM[self.how_to] != 'origin':
            raise ValueError('Already a sparse hamiltonian.')
        self.n_qubits = n_qubits
        self.how_to = MODE['backend']
        return self

    def get_cpp_obj(self, hermitian=False):
        if not hermitian:
            if not hasattr(self, 'ham_cpp'):
                if self.how_to == MODE['origin']:
                    ham = mb.hamiltonian(self.ham_termlist)
                elif self.how_to == MODE['backend']:
                    ham = mb.hamiltonian(self.ham_termlist, self.n_qubits)
                else:
                    dim = self.sparse_mat.shape[0]
                    nnz = self.sparse_mat.nnz
                    csr_mat = mb.csr_hd_matrix(dim, nnz,
                                               self.sparse_mat.indptr,
                                               self.sparse_mat.indices,
                                               self.sparse_mat.data)
                    ham = mb.hamiltonian(csr_mat, self.n_qubits)
                self.ham_cpp = ham
            return self.ham_cpp
        if self.how_to == MODE['backend'] or self.how_to == MODE['origin']:
            return self.get_cpp_obj()
        if not hasattr(self, 'herm_ham_cpp'):
            herm_sparse_mat = self.sparse_mat.conjugate().T.tocsr()
            dim = herm_sparse_mat.shape[0]
            nnz = herm_sparse_mat.nnz
            csr_mat = mb.csr_hd_matrix(dim, nnz, herm_sparse_mat.indptr,
                                       herm_sparse_mat.indices,
                                       herm_sparse_mat.data)
            self.herm_ham_cpp = mb.hamiltonian(csr_mat, self.n_qubits)
        return self.herm_ham_cpp

    def generate_sparse_data(self, half=True):
        """
        Generate non zero data, col and row data for sparse matrix.

        Note:
            The sparse matrix are supposed to hermitian matrix.
        """
        if (half):
            mat_triu = sp.triu(self.sparse_mat, format='csr')
        else:
            mat_triu = self.sparse_mat
        self.sparse_mat_indptr = [int(i) for i in mat_triu.indptr]
        self.sparse_mat_indice = [int(i) for i in mat_triu.indices]
        if not mat_triu.dtype.name.startswith('complex'):
            raise ValueError(f"sparse matrix should be complex type")
        if mat_triu.dtype.name[7:] == '64':
            dtype = np.float32
        elif mat_triu.dtype.name[7:] == '128':
            dtype = np.float64
        else:
            raise TypeError(
                f"sparse matrix should be complex64 or complex128, but get{mat_triu.dtype}"
            )
        self.sparse_mat_data = [[
            float(i.real) for i in mat_triu.data.view(dtype)
        ], [float(i.imag) for i in mat_triu.data.view(dtype)]]

    def mindspore_data(self):
        """
        Generate hamiltonian information for PQC operator.
        """
        m_data = {
            "hams_pauli_coeff": [],
            "hams_pauli_word": [],
            "hams_pauli_qubit": [],
            "how_to": self.how_to,
            "hams_sparse_data": self.sparse_mat_data,
            "hams_sparse_indptr": self.sparse_mat_indptr,
            "hams_sparse_indice": self.sparse_mat_indice
        }
        for term, coeff in self.ham_termlist:
            m_data["hams_pauli_coeff"].append(float(coeff))
            m_data["hams_pauli_word"].append([])
            m_data["hams_pauli_qubit"].append([])
            for qubit, word in term:
                m_data["hams_pauli_qubit"][-1].append(qubit)
                m_data["hams_pauli_word"][-1].append(word)
        return m_data


def _sparse_hamiltonian(qo, n):
    """Sparse hamiltonian"""
    if not isinstance(qo, (pq_operator, of_operator, hiq_operator)):
        raise TypeError("Require a QubitOperator, but get {}!".format(
            type(qo)))
    out = []
    for term, coeff in tqdm.tqdm(qo.terms.items()):
        out.append(_sparse_term(term, coeff, n))
    step = _get_contraction_time(len(out))
    for _ in tqdm.tqdm(range(step)):
        for i in range(len(out))[::2]:
            if i != len(out) - 1:
                out[i] += out[i + 1]
        out = out[::2]
    return sp.coo_matrix(out[0])


def _get_contraction_time(b, o=0):
    """Get contraction time"""
    if b == 1:
        return o
    return _get_contraction_time(b // 2 + b % 2, o + 1)


def _sparse_term(term, coeff, n):
    """sparse term"""
    tot = [sp.csr_matrix(I.matrix()) for i in range(n)]
    if term:
        for i, p in term:
            if i >= n:
                raise ValueError(f'hamiltonian term {p} not in qubit range.')
            tot[-i - 1] = sp.csr_matrix(MAT_MAP[p])
    tot[-1] *= coeff
    return _kron_n(tot)


def _kron_n(m):
    """kron multiplier sparse matrix"""
    if len(m) == 1:
        return m[0]
    else:
        out = m[0]
        for i in m[1:]:
            out = sp.kron(out, i, 'coo')
        return out