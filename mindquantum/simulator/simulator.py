import numpy as np
from mindquantum.circuit import Circuit
from mindquantum.gate import Hamiltonian
from mindquantum.parameterresolver import ParameterResolver
from .. import mqbackend as mb


class Simulator:
    def __init__(self, backend, n_qubits, rnd=1):
        self.backend = backend
        if backend == 'projectq':
            self.sim = mb.projectq(rnd, n_qubits)

    def flush(self):
        if self.backend == 'projectq':
            self.sim.run()

    def apply_gate(self, gate, parameter_resolver=None):
        if parameter_resolver is None:
            if gate.isparameter:
                raise ValueError(
                    "apply a parameterized gate needs a parameter_resolver")
            self.sim.apply_gate(gate.get_cpp_obj())
        else:
            self.sim.apply_gate(gate.get_cpp_obj(),
                                parameter_resolver.get_cpp_obj(), False)

    def apply_circuit(self, circuit, parameter_resolver=None):
        if not isinstance(circuit, Circuit):
            raise TypeError(
                f"circuit must be Circuit, but get {type(Circuit)}")
        if parameter_resolver is None:
            if circuit.para_name:
                raise ValueError(
                    "Applying a parameterized circuit needs a parameter_resolver"
                )
            self.sim.apply_circuit(circuit.get_cpp_obj())
        else:
            if not isinstance(parameter_resolver, ParameterResolver):
                raise TypeError(
                    f"parameter_resolver requires a ParameterResolver, but get {type(parameter_resolver)}"
                )
            self.sim.apply_circuit(circuit.get_cpp_obj(),
                                   parameter_resolver.get_cpp_obj())

    def apply_hamiltonian(self, hamiltonian):
        if not isinstance(hamiltonian, Hamiltonian):
            raise TypeError(
                f"hamiltonian requires a Hamiltonian, but got {type(hamiltonian)}"
            )
        self.sim.apply_hamiltonian(hamiltonian.get_cpp_obj())

    def get_expectation(self, hamiltonian):
        return self.sim.get_expectation(hamiltonian.get_cpp_obj())

    def get_vec(self):
        return np.array(self.sim.get_vec())

    def hermitian_measure_with_grad(self,
                                    circ,
                                    hams,
                                    parameter_resolvers,
                                    parallel_worker=None):
        n_meas = len(hams)
        n_prs = len(parameter_resolvers)
        batch_threads, mea_threads = _thread_balance(n_prs, n_meas,
                                                     parallel_worker)
        f_g = self.sim.hermitian_measure_with_grad(
            [i.get_cpp_obj() for i in hams], circ.get_cpp_obj(),
            circ.get_cpp_obj(hermitian=True),
            [i.get_cpp_obj() for i in parameter_resolvers],
            list(parameter_resolvers[0].keys()), batch_threads, mea_threads)
        f_g = np.array(f_g)
        f = np.real(f_g[:, :, 0])
        g = np.real(f_g[:, :, 1:])
        return f, g

    def nonhermitian_measure_with_grad(self,
                                       circ1,
                                       circ2,
                                       hams,
                                       parameter_resolvers,
                                       parallel_worker=None):
        n_meas = len(hams)
        n_prs = len(parameter_resolvers)
        batch_threads, mea_threads = _thread_balance(n_prs, n_meas,
                                                     parallel_worker)
        f_g = self.sim.nonhermitian_measure_with_grad(
            [i.get_cpp_obj() for i in hams], [i.get_cpp_obj(hermitian=True) for i in hams],
            circ1.get_cpp_obj(), circ1.get_cpp_obj(hermitian=True),
            circ2.get_cpp_obj(), circ2.get_cpp_obj(hermitian=True),
            [i.get_cpp_obj() for i in parameter_resolvers],
            list(parameter_resolvers[0].keys()), batch_threads, mea_threads)
        f_g = np.array(f_g)
        f = np.real(f_g[:, :, 0])
        g = np.real(f_g[:, :, 1:])
        return f, g


def _thread_balance(n_prs, n_meas, parallel_worker):
    if parallel_worker is None:
        parallel_worker = n_meas * n_prs
    if n_meas * n_prs <= parallel_worker:
        batch_threads = n_prs
        mea_threads = n_meas
    else:
        if n_meas < n_prs:
            batch_threads = min(n_prs, parallel_worker)
            mea_threads = min(n_meas, max(1, parallel_worker // batch_threads))
        else:
            mea_threads = min(n_meas, parallel_worker)
            batch_threads = min(n_prs, max(1, parallel_worker // mea_threads))
    return batch_threads, mea_threads
