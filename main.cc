#include <iostream>
#include <memory>
#include <type_traits>
#include <typeinfo>

#include "backends/projectq/projectq.h"
#include "gate/gates.h"
#include "utils.h"
using namespace mindquantum::projectq;
using namespace mindquantum;

int main() {
  Projectq<MT> p(1, 1);
  auto gate = GetGateByName<MT>("H");
  gate.base_matrix_.PrintInfo();
  gate.obj_qubits_.push_back(0);
  p.ApplyGate(gate);
  p.run();
  std::cout << "vec:" << p.vec_[0] << "," << p.vec_[1] << std::endl;
  VT<Index> ctrl = {0, 1, 3};
  p.InitializeSimulator();
}
