#!/bin/bash
# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

BASEPATH=$(cd "$(dirname $0)"; pwd)
OUTPUT_PATH="${BASEPATH}/output"
BUILD_PATH="${BASEPATH}/build/build"
DEP_PATH="${BASEPATH}/build/_deps"
PATCH_PATH="${BASEPATH}/third_party/patch"
PYTHON=$(which python3)
V2=`${PYTHON} -V 2>&1|awk '{print $2}'|awk -F '.' '{print $2}'`

mkdir -pv "${DEP_PATH}"
cd ${DEP_PATH}

if [ ! -f "v0.5.1.tar.gz" ];then
    echo "Downloading projectq from gitee"
    wget "https://gitee.com/mirrors/ProjectQ/repository/archive/v0.5.1.tar.gz" --no-check-certificate
else
    echo "projectq already download."
fi

if [ ! -d "projectq" ]; then
    tar -xf v0.5.1.tar.gz
    mv ProjectQ-v0.5.1 ProjectQ-0.5.1
    echo "patching ..."
    patch -p0 < ${PATCH_PATH}/projectq/projectq.patch001
    mv ProjectQ-0.5.1 projectq
fi

if [ $V2 -eq 7 ]; then
    PYBIND11URL="https://gitee.com/mirrors/pybind11/repository/archive/v2.4.3.tar.gz"
    PYBIND11VERSION="v2.4.3"
else
    PYBIND11URL="https://gitee.com/mirrors/pybind11/repository/archive/v2.6.1.tar.gz"
    PYBIND11VERSION="v2.6.1"
fi

if [ ! -f "${PYBIND11VERSION}.tar.gz" ];then
    echo "Downloading pybind11 from gitee"
    wget "${PYBIND11URL}" --no-check-certificate
else
    echo "pybind11 already download."
fi

if [ ! -d "pybind11" ];then
    tar -xf ${PYBIND11VERSION}.tar.gz
    mv pybind11-${PYBIND11VERSION} pybind11
fi

# rm -r ${BUILD_PATH}
mkdir -pv "${BUILD_PATH}"
cd ${BUILD_PATH}
cmake ${BASEPATH}
make -j6

cp ${BASEPATH}/build/mqbackend*.so ${BASEPATH}/tests/st/test_interface
cp ${BASEPATH}/build/mqbackend*.so ${BASEPATH}/mindquantum/
${PYTHON} ${BASEPATH}/tests/st/test_interface/test_interface.py
# mk_new_dir() {
#     local create_dir="$1"  # the target to make

#     if [[ -d "${create_dir}" ]];then
#         rm -rf "${create_dir}"
#     fi

#     mkdir -pv "${create_dir}"
# }

# write_checksum() {
#     cd "$OUTPUT_PATH" || exit
#     PACKAGE_LIST=$(ls mindquantum-*.whl) || exit
#     for PACKAGE_NAME in $PACKAGE_LIST; do
#         echo $PACKAGE_NAME
#         sha256sum -b "$PACKAGE_NAME" >"$PACKAGE_NAME.sha256"
#     done
# }

# mk_new_dir "${OUTPUT_PATH}"

# ${PYTHON} ${BASEPATH}/setup.py bdist_wheel

# mv ${BASEPATH}/dist/*whl ${OUTPUT_PATH}

# write_checksum


# echo "------Successfully created mindquantum package------"
