from mindquantum.simulator import Simulator
from mindquantum import *
from mindquantum import ParameterResolver
import numpy as np
from mindquantum import Circuit
from mindquantum.gate import RY, SWAPGate
from scipy.sparse import csr_matrix

circ1 = Circuit()
circ1 += RY('p1').on(0)
circ1 += SWAPGate().on([0, 1])
circ2 = Circuit()

pr = ParameterResolver({'p1': np.pi})

s = Simulator('projectq', 2)
zero_state = np.array([1, 0, 0, 0], dtype=complex)
target_state = np.array([0, 0, 1, 0], dtype=complex)
target_state_ham = zero_state.reshape(4, 1).dot(target_state.reshape(1, 4))
hams = Hamiltonian(csr_matrix(target_state_ham))
f, g = s.nonhermitian_measure_with_grad(circ2, circ1, [hams], [pr])

print(f, g)