# import mqbackend as mb
# import numpy as np
# import scipy.sparse as sp
# print(mb)

# pr = mb.parameter_resolver(['a', 'b', 'c'], [0.1, 0.2, 0.3], [0, 1, 0])
# assert pr.data == {'a': 0.1, 'b': 0.2, 'c': 0.3}
# assert pr.requires_grad_parameters == set(['b'])
# assert pr.no_grad_parameters == set(['a', 'c'])

# pr2 = mb.parameter_resolver(['a', 'c'], [1.2, 3.4], [1, 1])

# assert mb.linear_combine(pr, pr2) == 1.2 * 0.1 + 3.4 * 0.3

# x = np.array([[0, 0, 3, 0], [2, 3, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0]],
#              dtype=np.complex128)
# x = sp.csr_matrix(x)
# csr = mb.csr_hd_matrix(4, x.nnz, x.indptr, x.indices, x.data)
# ham = mb.hamiltonian(csr, 2)
# csr.PrintInfo()
# csr2 = mb.csr_plus_csr(csr, csr)
# csr2.PrintInfo()
# csr3 = mb.csr_plus_csr(csr, csr2)
# csr3.PrintInfo()
# csr4 = mb.transpose_csr_hd_matrix(csr3)
# csr4.PrintInfo()

# pt = (((0, 'X'), (1, 'Y')), 0.4)
# n_qubits = 2
# pm = mb.pauli_mat(pt, n_qubits)
# pm.PrintInfo()
# pm2 = mb.get_pauli_mat(pt, n_qubits)
# pm2.PrintInfo()

# csr5 = mb.pauli_mat_to_csr_hd_matrix(pm)
# csr5.PrintInfo()

# csr6 = mb.pauli_mat_to_csr_hd_matrix(pm2)
# csr6.PrintInfo()

# csr7 = mb.csr_plus_csr(csr5, csr6)
# csr7.PrintInfo()

# pt1 = ((), 0.4)
# pt2 = ((), -3.2)
# pt3 = ((), 6.4)
# n_qubits = 2
# csr8 = mb.sparse_hamiltonian([pt1, pt2, pt3], n_qubits)
# csr8.PrintInfo()

# gate1 = mb.get_gate_by_name('RX')
# gate1.obj_qubits = [0]
# gate1.PrintInfo()
# gate1.apply_value(1.1)
# gate1.PrintInfo()

# sim = mb.projectq(1, 1)
# sim.apply_gate(gate1)
# sim.run()
# sim.PrintInfo()

# sim2 = mb.projectq(1, 1)
# sim2.apply_circuit([gate1])
# sim2.PrintInfo()

from mindquantum.simulator import Simulator
from mindquantum import *
from mindquantum.ops import QubitOperator
from mindquantum.circuit import generate_uccsd
from mindquantum.nn import MindQuantumAnsatzOnlyOperator
import numpy as np
import time
from scipy.optimize import minimize

datas = generate_uccsd("/home/xuxs/gitee/mindquantum/tests/st/LiH.hdf5")
pr = ParameterResolver(dict(zip(datas[2], datas[1])))
circ = UN(X, datas[-1]) + datas[0]
ham = Hamiltonian(datas[3]).sparse(datas[-2])
hams = [ham, ham]
prs = [pr, pr]
s = Simulator('projectq', datas[-2])
f, g = s.hermitian_measure_with_grad(circ, hams, prs)
# t0 = time.time()
# sim = Simulator('projectq', datas[-2])
# f_g = sim.sim.evol_with_grad([ham.get_cpp_obj()], circ.get_cpp_obj(),
#                              herm_circ.get_cpp_obj(), [pr.get_cpp_obj()])
# print(f_g)
# print(time.time() - t0)

# def fun(p):
#     pr = ParameterResolver(dict(zip(datas[2], p)))
#     sim = Simulator('projectq', datas[-2])
#     f_g = sim.sim.hermitian_measure_with_grad(
#         [ham.get_cpp_obj(),
#          ham.get_cpp_obj(),
#          ham.get_cpp_obj()], circ.get_cpp_obj(), herm_circ.get_cpp_obj(),
#         [pr.get_cpp_obj(), pr.get_cpp_obj()], list(pr.keys()), 2, 2)
#     # f_g = np.real(np.array(f_g[0]))
#     # f = f_g[0]
#     # g = f_g[1:]
#     # print(f)
#     print(f_g)
#     # return f, g
#     return f_g

# for i in range(40):
#     fun(np.zeros(len(datas[1])))
# res = minimize(fun, np.zeros(len(datas[1])), method='bfgs', jac=True)

# sim = Simulator('projectq', 1)
# c = Circuit().ry('a', 0)
# pr = ParameterResolver({'a': 0.8})
# sim.apply_circuit(c, pr)
# # sim.apply_gate(c[0], pr)
# # sim.apply_circuit(c.hermitian)
# print(sim.get_vec())
# ham = Hamiltonian(QubitOperator('Z0')).sparse(1)
# print(sim.get_expectation(ham))
# q = QubitOperator('X0 Y1', 0.4)
# ham = Hamiltonian(q)
# ham.sparse(2)
# ham_cpp = ham.get_cpp_obj()
# sim.sim.apply_hamiltonian(ham_cpp)
# print(sim.sim.get_vec())
# c = Circuit().h(0).rx(0.4, 0)
# c_cpp = [i.get_cpp_obj() for i in c]
# sim.apply_circuit(c_cpp)
