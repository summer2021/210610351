numpy >= 1.17.0
scipy >= 1.5.3
projectq >= 0.5.1
openfermion>=1.0.0
qutip>=4.5.3
sympy >= 1.4
matplotlib >= 3.1.3
tqdm